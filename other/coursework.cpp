/*
	For a given natural number N 
	find the minimum natural number M 
	that is greater and 
	has a sum of digits 
	equal to the sum of the digits of N
*/


#include <stdio.h>
#include <string.h>

int main()
{
	char n[100];
	int m[100];
	int i, j, k, br;

	printf("Enter the number: ");
	gets_s(n);

	br = strlen(n);

	m[0] = 0;

	for (i = 1; i <= br; i++)
	{
		m[i] = n[i - 1] - 48;
	}

	i = br;

	while (!m[i])
	{
		i--;
	}

	m[i]--;
	i--;

	while (m[i] == 9)
	{
		i--;
	}

	m[i]++;

	for (k = i + 1, j = br; k<j; k++, j--)
	{
		while (m[k]>0 && m[j] < 9)
		{
			m[k]--;
			m[j]++;
		}
	}

	printf("The number is: ");

	if (m[0])
	{
		printf("%d", m[0]);
	}

	for (i = 1; i <= br; i++)
	{
		printf("%d", m[i]);
	}

	fflush(stdin);
	getchar();
	return 0;

}
