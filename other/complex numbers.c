#include <stdio.h>
#include <stdlib.h>

#define N 3

typedef struct
{
	int Re;
	int Im;
}COMPLEX;

COMPLEX add(COMPLEX *numbers);
COMPLEX substract(COMPLEX *numbers);
COMPLEX multiply(COMPLEX *numbers);

int main()
{
	int i, choice;
	COMPLEX *numbers;
	COMPLEX result;

	numbers = (COMPLEX*)malloc(N*sizeof(COMPLEX));

	for (i = 0; i<N; i++)
	{
		fflush(stdin);
		printf("%d) Re = ", i+1);
		scanf_s("%d", &numbers[i].Re);
		printf("%d) Im = ", i+1);
		scanf_s("%d", &numbers[i].Im);
	}

	while (1)
	{
		printf("\n\n1) Add the complex numbers.\n");
		printf("2) Subtract the complex numbers.\n");
		printf("3) Multiply the complex numbers.\n");
		printf("0) Exit.\n");
		printf("Enter your choice: ");
		scanf_s("%d", &choice);

		switch (choice)
		{
			case 1: 
				result = add(numbers);
				break;

			case 2:
				result = substract(numbers);
				break;

			case 3:
				result = multiply(numbers);
				break;

			case 0:
				return 0;
				break;
				
			default:
				printf("Wrong number\n");
				continue;
		}

		printf("\n\nresult = (%d%+di)\n\n", result.Re, result.Im);
		
	}

	return 0;
}

COMPLEX add(COMPLEX *numbers)
{
	COMPLEX result = { 0, 0 };
	int i;

	for (i = 0; i<N; i++)
	{
		result.Re += numbers[i].Re;
		result.Im += numbers[i].Im;
	}

	return result;
}

COMPLEX substract(COMPLEX *numbers)
{
	COMPLEX result = { 0, 0 };
	int i;

	result = numbers[0];

	for (i = 1; i<N; i++)
	{
		result.Re -= numbers[i].Re;
		result.Im -= numbers[i].Im;
	}

	return result;
}

COMPLEX multiply(COMPLEX *numbers)
{
	COMPLEX result = { 0, 0 };
	int temp;
	int i;

	result = numbers[0];

	for (i = 1; i<N; i++)
	{
		temp = result.Re;

		result.Re = (result.Re * numbers[i].Re) - (result.Im * numbers[i].Im);
		result.Im = (temp * numbers[i].Im) + (result.Im * numbers[i].Re);
	}

	return result;
}
