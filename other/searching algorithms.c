#include <stdio.h>

#define N 15

int binary_searching(int *array, int x);
int sequential_searching(int *array, int x);
int sequential_searching_sorted(int A[], int x);

int main()
{
	int A[N] = { 2, 9, 6, 69, 54, 32, 87, 96, 0, 45, 76, 23, 13, 19, 56 };
	int B[N] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16 };
	int x;

	printf("Binary searching. Enter a number: ");
	scanf_s("%d", &x);
	printf("Index of %d is %d", x, binary_searching(B, x));

	printf("\n\nSequential searching. Enter a number: ");
	scanf_s("%d", &x);
	printf("Index of %d is %d", x, sequential_searching(A, x));
	
	printf("\n\nSequential searching in sorted array. Enter a number: ");
	scanf_s("%d", &x);
	printf("Index of %d is %d", x, sequential_searching_sorted(B, x));
	
	fflush(stdin);
	getchar();
	return 0;
}

int binary_searching(int *array, int x)
{
	int left = 0, right = N - 1, middle;

	while (left <= right)
	{
		middle = (left + right) / 2;
		if (x < array[middle])
		{
			right = middle - 1;
		}
		
		else if (x > array[middle])
		{
			left = middle + 1;
		}
		else
		{
			return middle;
		}
	}
	return EOF;
}

int sequential_searching(int *array, int x)
{
	int i;

	for (i = 0; i < N; i++)
	{
		if (array[i] == x)
		{
			return i;
		}
	}
	return EOF;
}

int sequential_searching_sorted(int *array, int x)
{
	int i;

	for (i = 0; array[i] <= x && i < N; i++)
	{
		if (array[i] == x)
		{
			return i;
		}
	}
	return EOF;
}

