#include <stdio.h>

int fibonacci_numbers(int n);
int factorial(int n);

int main()
{
	int n, i;

	printf("Enter n : ");
	scanf_s("%d", &n);

	printf("Fibbonacci of %d: ", n);
	for (i = 0; i < n; i++) {
		printf("%d ", fibonacci_numbers(i));
	}
	
	printf("\nFactorial of %d = %lu", n, factorial(n));

	fflush(stdin);
	getchar();
	return 0;

}

int fibonacci_numbers(int n)
{
	if (n <= 1)
	{
		return 1;
	}
	else
	{
		return fibonacci_numbers(n - 1) + fibonacci_numbers(n - 2);
	}
}

int factorial(int n)
{
	if (!n)
	{
		return 1;
	}
	else
	{
		return n*factorial(n - 1);
	}
}


