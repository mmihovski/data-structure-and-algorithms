#include <stdio.h>
#include <stdlib.h>

struct node
{
	char bracket;
	struct node *next;
};

struct node *top = NULL;

void push(char c);
int check_stack(char c);

int main()
{
	char buffer[100];
	int i, flag;

	printf("Enter string: ");
	fgets(buffer, 100, stdin);

	for (i = 0; buffer[i]; i++)
	{
		if (buffer[i] == '(' || buffer[i] == '[' || buffer[i] == '{')
		{
			push(buffer[i]);
		}
		else if (buffer[i] == ')' || buffer[i] == ']' || buffer[i] == '}')
		{
			flag = check_stack(buffer[i]);

			if (flag == 0)
			{
				break;
			}
		}
	}

	if (top)
	{
		flag = 0;
	}

	if (flag)
	{
		printf("Brackets are balanced");
	}
	else
	{
		printf("Brackets are not balanced");
	}

	fflush(stdin);
	getchar();
	return 0;
}

void push(char bracket)
{
	struct node *p;
	p = (struct node *)malloc(sizeof(struct node));
	
	p->bracket = bracket;
	p->next = top;
	top = p;
}

int check_stack(char bracket)
{
	char mirror_bracket;
	struct node *p;

	switch (bracket)
	{
		case ')':
			mirror_bracket = '(';
			break;
		case '}':
			mirror_bracket = '{';
			break;
		case ']':
			mirror_bracket = '[';
			break;
	}

	if (top && top->bracket == mirror_bracket)
	{
		p = top;
		top = top->next;
		free(p);
		return 1;
	}
	else
	{
		return 0;
	}
}
