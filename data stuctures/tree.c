﻿#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 9

struct node
{
	int key;
	struct node *left;
	struct node *right;
};

struct node * build_tree(int A[], int left, int right);

void prefix(struct node *p);
void postfix(struct node *p);
void infix(struct node *p);
struct node * binary_seacrh(int x, struct node *p);

int depth(struct node *root);
int max_element(int x, int y);
int count(struct node *root);

int is_balanced(struct node *root);
int is_ideal_balanced(struct node *root);

int main()
{
	int array[N] = { 1, 2, 3, 4, 5, 6, 7, 8, 9};
	int x;
	struct node *root = NULL;
	struct node *p = NULL;

	root = build_tree(array, 0, N - 1);

	printf("Prefix: ");
	prefix(root);
	printf("\nPostfix: ");
	postfix(root);
	printf("\nInfix: ");
	infix(root);

	printf("\n\nHeight of the tree is: %d", depth(root));
	printf("\nLeaf count of the tree is %d", count(root));

	x = is_balanced(root);
	if (x)
	{
		printf("\nThe tree is balanced!");
	}
	else
	{
		printf("\nThe tree is not balanced!");
	}

	x = is_ideal_balanced(root);
	if (x)
	{
		printf("\nThe tree is ideal balanced!");
	}
	else
	{
		printf("\nThe tree is not ideal balanced!");
	}

	printf("\n\nSearch element: ");
	scanf_s("%d", &x);
	p = binary_seacrh(x, root);
	if (p)
	{
		printf("Searched element is: %d ", p->key);
	}
	else
	{
		printf("Not finded: ");
	}

	fflush(stdin);
	getchar();
	return 0;
}


struct node * build_tree(int *array, int left, int right)
{
	int middle;
	struct node *p = NULL;

	middle = (left + right) / 2;

	if (left <= right)
	{
		p = (struct node *)malloc(sizeof(struct node));
		p->key = array[middle];
		p->left = build_tree(array, left, middle - 1);
		p->right = build_tree(array, middle + 1, right);
	}

	return p;
}

void prefix(struct node *p)
{
	if (p)
	{
		printf("%d ", p->key);
		prefix(p->left);
		prefix(p->right);
	}
}

void infix(struct node *p)
{
	if (p)
	{
		prefix(p->left);
		printf("%d ", p->key);
		prefix(p->right);
	}
}

void postfix(struct node *p)
{
	if (p)
	{
		prefix(p->left);
		prefix(p->right);
		printf("%d ", p->key);
	}
}

struct node * binary_seacrh(int x, struct node *p)
{
	if (p != NULL)
	{
		if (x < p->key)
		{
			binary_seacrh(x, p->left);
		}
		else if (x > p->key)
		{
			binary_seacrh(x, p->right);
		}
		else
		{
			return p;
		}
	}
	else
	{
		return NULL;
	}
}


int depth(struct node *root)
{
	if (root != NULL)
	{
		return (1 + max_element(depth(root->left), depth(root->right)));
	}
	else
	{
		return 0;
	}
}

int max_element(int x, int y)
{
	return (x>y) ? x : y;
}

int count(struct node *root)
{
	if (root != NULL)
	{
		return (1 + count(root->left) + count(root->right));
	}
	else
	{
		return 0;
	}
}

int is_balanced(struct node *root)
{
	int k;

	if (root)
	{
		k = abs(depth(root->left) - depth(root->right));
		return k <= 1 && is_balanced(root->left) && is_balanced(root->right);
	}
	else
	{
		return 1;
	}
}

int is_ideal_balanced(struct node *root)
{
	int k;

	if (root)
	{
		k = abs(count(root->left) - count(root->right));
		return k <= 1 && is_ideal_balanced(root->left) && is_ideal_balanced(root->right);
	}
	else
	{
		return 1;
	}
}