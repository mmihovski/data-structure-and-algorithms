#include <stdio.h>
#include <stdlib.h>

#define N 3

struct node
{
	int data;
	struct node *next;
};

void push(struct node **top, struct node **tail, int data);
int pop(struct node **top);

int main()
{
	int i;
	int data;
	struct node *top = NULL, *tail = NULL;

	printf("Enter data: \n");
	for (i = 0; i<N; i++)
	{
		printf("%d): ", i + 1);
		scanf_s("%d", &data);
		push(&top, &tail, data);
	}

	printf("\n");

	for (i = 0; top; i++)
	{
		printf("%d) %d\n", i + 1,  pop(&top));
	}

	printf("\n\n");
	system("pause");
	return 0;
}

void push(struct node **top, struct node **tail, int data)
{
	struct node *p;

	p = (struct node*) malloc(sizeof(struct node));

	p->data = data;
	p->next = NULL;

	if (!*tail)
	{
		*top = p;
	}
	else
	{
		(*tail)->next = p;
	}

	*tail = p;
}

int pop(struct node **top)
{
	int data;
	struct node *temp;

	data = (*top)->data;

	temp = *top;
	*top = (*top)->next;

	free(temp);

	return data;
}

