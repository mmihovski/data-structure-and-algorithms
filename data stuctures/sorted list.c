#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 5

typedef struct
{
	int fnum;
	char name[30];
}STUDENT;

struct node
{
	STUDENT student;
	struct node *next;
};

void push(struct node **top, STUDENT student);
STUDENT pop(struct node **top);
void display_data(struct node *top);
struct node *sequential_searching(int number, struct node *head);

int main()
{
	struct node *top = NULL;
	struct node *p;
	int number, i;
	STUDENT *student;

	student = (STUDENT *)malloc(N * sizeof(STUDENT));

	for (i = 0; i < N; i++)
	{
		fflush(stdin);
		printf("Enter number: ");
		scanf_s("%d", &student[i].fnum);
		printf("Enter name: ");
		fflush(stdin);
		fgets(student[i].name, 30, stdin);
		push(&top, student[i]);
	}
	
	free(student);

	printf("\n");

	display_data(top);
	
	printf("\nSearch student by number: ");
	scanf_s("%d", &number);

	p = sequential_searching(number, top);
	if (p)
	{
		printf("Student name is %s\n\n", p->student.name);
	}
	else
	{
		printf("�he student was not found");
	}
	
	fflush(stdin);
	getchar();
	return 0;
}

void push(struct node **top, STUDENT student)
{
	struct node *new_node;
	struct node *place;

	new_node = (struct node*) malloc(sizeof(struct node));

	new_node->student.fnum = student.fnum;
	strcpy_s(new_node->student.name, strlen(student.name) + 1, student.name);

	if (!*top || student.fnum > (*top)->student.fnum)
	{
		new_node->next = *top;
		*top = new_node;
	}
	else
	{
		place = *top;

		while (place->next && place->next->student.fnum > student.fnum)
		{
			place = place->next;
		}

		new_node->next = place->next;
		place->next = new_node;
	}
}

STUDENT pop(struct node **top)
{
	STUDENT student;
	struct node *temp;

	student = (*top)->student;
	temp = *top;
	*top = (*top)->next;
	free(temp);

	return student;
}

void display_data(struct node *top)
{
	int i;
	STUDENT student;
	struct node *p;

	p = top;

	printf("Sorted list: \n");
	for (i = 0; p; i++)
	{
		student = p->student;
		printf("%d) FNUM = %d\n", i + 1, student.fnum);
		printf("%d) NAME = %s\n", i + 1, student.name);
		p = p->next;
	}
}

struct node *sequential_searching(int number, struct node *head)
{
	while (head && head->student.fnum > number)
	{
		head = head->next;
	}
	if (!head || head->student.fnum != number)
	{
		return NULL;
	}
	else
	{
		return head;
	}
}



