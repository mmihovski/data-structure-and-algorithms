#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

struct node
{
	int rank;
	char task[20];
	struct node *next;
};

struct node *head = NULL;

struct node *find_place(int rank);
void add_item(int rank, char *task);
void print_task_queue();

int main()
{
	int rank;
	char buffer[20];
	char choice;

	do
	{
		printf("Enter rank : ");
		scanf_s("%d", &rank);
		fflush(stdin);
		printf("Enter task : ");
		fgets(buffer, 20, stdin);

		printf("\nAdd more task ? Y/N: ");
		scanf_s("%c", &choice);

		printf("\n");

		add_item(rank, buffer);

	}while('N' != toupper(choice));
	//}while (c == 'Y' || c == 'y');

	print_task_queue();

	fflush(stdin);
	getchar();
	return 0;
}

struct node *find_place(int rank)
{
	struct node *p = head, *place = NULL;

	while (p && p->rank > rank)
	{
		place = p;
		p = p->next;
	}

	return place;
}

void add_item(int rank, char *task)
{
	struct node *new_task;
	struct node *place;

	new_task = (struct node *) malloc(sizeof(struct node));
	new_task->rank = rank;
	strcpy_s(new_task->task, strlen(task) + 1, task);

	place = find_place(rank);

	if (!place)
	{
		new_task->next = head;
		head = new_task;
	}
	else
	{
		new_task->next = place->next;
		place->next = new_task;
	}
}

void print_task_queue()
{
	struct node *p = head;

	printf("Tasks: \n");
	while (p)
	{
		printf("%d :  %s", p->rank, p->task);
		p = p->next;
	}
}
