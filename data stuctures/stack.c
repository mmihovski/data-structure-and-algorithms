#include <stdio.h>
#include <stdlib.h>

#define MAX 5

struct node
{
	int data;
	struct node *next;
};

void push(struct node **top);
void pop(struct node **top);
void show(struct node **top);
void empty(struct node **top);

int count;

int main()
{
	struct node *top = NULL;
	int choice;

	count = 0;

	while (1)
	{
		system("cls");

		printf("1) Push data in the stack\n");
		printf("2) Pop data from the stack\n");
		printf("3) Show data from the stack\n");
		printf("4) Empty the stack.\n");
		printf("0) Exit.\n\n");
		printf("Enter your choice: ");
		scanf_s("%d", &choice);

		switch (choice)
		{
		case 1:
			push(&top);
			break;

		case 2:
			pop(&top);
			break;

		case 3:
			show(&top);
			break;

		case 4:
			empty(&top);
			break;

		case 0:
			empty(&top);
			return 0;
			break;

		default:
			printf("Wrong number\n");
			continue;
		}

		printf("\nPress Enter to continue");
		fflush(stdin);
		getchar();
	}

	fflush(stdin);
	getchar();
	return 0;
}

void push(struct node **top)
{
	int data;
	struct node *p;
	p = (struct node*) malloc(sizeof(struct node));

	if (count >= MAX)
	{
		printf("\nStack is full\n");
		return;
	}

	printf("\nEnter data: ");
	scanf_s("%d", &data);

	p->data = data;
	p->next = *top;
	*top = p;

	count++;
}

void pop(struct node **top)
{
	int data;
	struct node *temp;

	if (count == 0)
	{
		printf("\nStack is empty\n");
		return;
	}
	data = (*top)->data;

	printf("\n%d) = %d\n", count, data);

	temp = *top;
	*top = (*top)->next;
	free(temp);

	count--;
}

void show(struct node **top)
{
	int data, i;
	struct node *p = NULL;

	if (!(*top))
	{
		printf("\nStack is empty\n");
		return;
	}

	p = *top;

	printf("\n");
	for (i = 0; p; i++)
	{
		data = p->data;
		printf("%d) = %d\n", i + 1, data);
		p = p->next;
	}
}

void empty(struct node **top)
{
	int i;
	struct node *p = NULL;

	if (!(*top))
	{
		printf("\nStack is empty\n");
		return;
	}

	for (i = 0; *top; i++)
	{
		p = *top;
		*top = (*top)->next;
		free(p);
		count--;
	}
}

