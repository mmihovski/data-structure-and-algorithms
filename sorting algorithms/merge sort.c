#include <stdio.h>

#define N 10

int a[N] = { 2, 6, 8, 1, 3, 7, 4, 9, 5, 0 };
int b[N + 1];

void sort(int l, int r);
void merging(int l, int m, int r);

int main()
{
	int i;

	printf("List before sorting\n");

	for (i = 0; i < N; i++)
		printf("%d ", a[i]);

	sort(0, N - 1);

	printf("\n\nList after sorting\n");

	for (i = 0; i < N; i++)
		printf("%d ", a[i]);


	fflush(stdin);
	getchar();
	return 0;
}

void sort(int left, int right)
{
	int middle;

	if (left < right)
	{
		middle = (left + right) / 2;
		sort(left, middle);
		sort(middle + 1, right);

		merging(left, middle, right);
	}
}

void merging(int left, int middle, int right)
{
	int i1, i2, j1, j2, k;

	k = left;
	i1 = left;
	i2 = middle;
	j1 = middle + 1;
	j2 = right;

	while (i1 <= i2 && j1 <= j2)
	{
		if (a[i1] < a[j1])
		{
			b[k++] = a[i1++];
		}
		else
		{
			b[k++] = a[j1++];
		}
	}

	while (i1 <= i2)
	{
		b[k++] = a[i1++];
	}

	while (j1 <= j2)
	{
		b[k++] = a[j1++];
	}

	for (k = left; k <= right; k++)
	{
		a[k] = b[k];
	}
}




