#include <stdio.h>

#define N 10
#define M 10 

int main()
{
	int i, j;
	int array[N] = { 2, 0, 2, 1, 4, 2, 9, 4, 5, 8 };
	int count[M + 1] = { 0 };

	for (i = 0; i < N; i++)
	{
		printf("%d ", array[i]);
	}
	printf("\n");

	for (i = 0; i < N; i++)
	{
		count[array[i]]++;
	}
	
	for (i = 0, j = 0; i < N; j++)
	{
		if (count[j])
		{
			while (count[j])
			{
				array[i++] = j;
				count[j]--;
			}
		}
		
	}

	for (i = 0; i < N; i++)
	{
		printf("%d ", array[i]);
	}


	fflush(stdin);
	getchar();
	return 0;
}