#include <stdio.h>

#define N 10

void heapify(int *array, int n, int i);
void heap_sort(int *array, int n);
void swap(int *a, int *b);

int main()
{
	int array[] = { 2, 6, 9, 4, 1, 0, 3, 5, 8, 7}, i;

	heap_sort(array, N);

	for (i = 0; i < N; i++)
	{
		printf("%d ", array[i]);
	}

	fflush(stdin);
	getchar();
	return 0;
}

void swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void heapify(int *array, int n, int i)
{
	int largest = i;
	int left = 2 * i + 1;
	int right = 2 * i + 2;

	if (left < n && array[left] > array[largest])
	{
		largest = left;
	}

	if (right < n && array[right] > array[largest])
	{
		largest = right;
	}

	if (largest != i) 
	{
		swap(&array[i], &array[largest]);
		heapify(array, n, largest);
	}
}

void heap_sort(int *array, int n) 
{
	for (int i = n / 2 - 1; i >= 0; i--)
	{
		heapify(array, n, i);
	}

	for (int i = n - 1; i >= 0; i--) 
	{
		swap(&array[0], &array[i]);
		heapify(array, i, 0);
	}
}
