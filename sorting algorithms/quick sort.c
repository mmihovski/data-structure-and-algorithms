#include <stdio.h>

#define N 10

void quick_sort(int *array, int start, int stop);

int main()
{
	int array[] = { 1, 4, 3, 7, 2, 0, 9, 8, 5, 6 };
	int i;

	quick_sort(array, 0, N - 1);

	for (i = 0; i < N; i++)
	{
		printf("%d ", array[i]);
	}

	fflush(stdin);
	getchar();
	return 0;
}

void quick_sort(int *array, int start, int stop)
{
	int i = start, j = stop, m, temp;
	m = array[(i + j) / 2];

	while (i <= j)
	{
		while (array[i] < m)
		{
			i++;
		}

		while (array[j] > m)
		{
			j--;
		}

		if (i <= j)
		{
			temp = array[j];
			array[j] = array[i];
			array[i] = temp;
			i++;
			j--;
		}
	}

	if (j > start)
	{
		quick_sort(array, start, j);
	}

	if (i < stop)
	{
		quick_sort(array, i, stop);
	}
}
