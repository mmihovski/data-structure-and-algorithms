#include <stdio.h>

#define N 9

void selection_sort(int *p);   
void insertion_sort(int *p);         
void bubble_sort(int *p);          
int main()
{
	int array1[N] = { 3, 7, 1, 4, 8, 5, 2, 9, 6 };
	int array2[N] = { 3, 7, 1, 4, 8, 5, 2, 9, 6 };
	int array3[N] = { 3, 7, 1, 4, 8, 5, 2, 9, 6 };
	int i;

	selection_sort (array1);
	insertion_sort(array2);
	bubble_sort (array3);

	printf("selection sort:\n");

	for (i = 0; i<N; i++)
		printf("%d ", array1[i]);

	printf("\n\ninsertion sort:\n");

	for (i = 0; i<N; i++)
		printf("%d ", array2[i]);

	printf("\n\nbubble sort:\n");

	for (i = 0; i<N; i++)
		printf("%d ", array3[i]);


	fflush(stdin);
	getchar();
	return 0;
}


void selection_sort(int *p)
{
	int i, j, x, min;

	for (i = 0; i<N - 1; i++)
	{
		min = i;

		for (j = i + 1; j < N; j++)
		{
			if (p[j] < p[min])
			{
				min = j;
			}
		}
		
		if (i != min)
		{
			x = p[min];
			p[min] = p[i];
			p[i] = x;
		}
	}
}



void insertion_sort(int *p)
{
	int i, j, x;

	for (i = 1; i<N; i++)
	{
		x = p[i];

		for (j = i - 1; j >= 0 && x < p[j]; j--)
		{
			p[j + 1] = p[j];
		}
		p[j + 1] = x;
	}
}


void bubble_sort(int *p)
{
	int i, j, x;

	for (i = 0; i<N - 1; i++)
	{
		for (j = N - 1; j>i; j--)
		{
			if (p[j] < p[j - 1])
			{
				x = p[j];
				p[j] = p[j - 1];
				p[j - 1] = x;
			}
		}
	}
}