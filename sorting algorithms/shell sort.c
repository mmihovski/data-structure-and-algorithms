#include <stdio.h>

#define N 10

void shellsort(int *array)
{
	int i, j, k, tmp;

	for (i = N / 2; i > 0; i = i / 2)
	{
		for (j = i; j < N; j++)
		{
			for (k = j - i; k >= 0; k = k - i)
			{
				if (array[k + i] >= array[k])
					break;

				else
				{
					tmp = array[k];
					array[k] = array[k + i];
					array[k + i] = tmp;
				}
			}
		}
	}
}

int main()
{
	int array[N] = { 6, 1, 7, 9, 2, 3, 8, 5, 4, 0 };
	int k;

	shellsort(array);

	printf("Sorted array is: ");

	for (k = 0; k < N; k++)
		printf("%d ", array[k]);

	fflush(stdin);
	getchar();
	return 0;
}
